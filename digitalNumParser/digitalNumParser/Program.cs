﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace digitalNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            //provides path to the input file for testing purposes
            //string inputFile = //uncomment and put path to your test file here

            //comment out the next three lines if you wish to use a different input test file
            string pathToAssembly = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string pathToResources = Path.Combine(Directory.GetParent(pathToAssembly).Parent.FullName);
            string inputFile = pathToResources + "\\accountNums.txt"; //if you add a test file to the project, just replace the accountNums.txt with the new test file name

            using (StreamReader inputStream = new StreamReader(inputFile))
            {
                string inputLine1;
                string inputLine2;
                string inputLine3;

                //get three lines from the input file that represent a single account number from the bank
                inputLine1 = inputStream.ReadLine();
                inputLine2 = inputStream.ReadLine();
                inputLine3 = inputStream.ReadLine();

                //make sure that each line is not null before we try to start grabbing substrings
                while (inputLine1 != null && inputLine2 != null && inputLine3 != null)
                {
                    char[] accountNum = new char[9]; //used for quick/easy checksum calculation
                    bool validAcctNum = false; //outputs validity of account number
                    StringBuilder sbDecodedAccountNum = new StringBuilder(); //used for storing the decoded account number

                    //Show digital account number we are decoding
                    Console.WriteLine("Digital Account Num:");
                    Console.WriteLine(inputLine1);
                    Console.WriteLine(inputLine2);
                    Console.WriteLine(inputLine3);

                    //store our now decoded account number in the stringbuilder obj
                    sbDecodedAccountNum = getChar(inputLine1, inputLine2, inputLine3, sbDecodedAccountNum);
                    Console.WriteLine("Decoded Account Num: ");
                    Console.WriteLine(sbDecodedAccountNum);

                    //copy stringbuilder object to char array
                    sbDecodedAccountNum.CopyTo(0, accountNum, 0, sbDecodedAccountNum.Length); 

                    validAcctNum = checkSum(accountNum);

                    //Because who doesn't like color coded console output for validation and a good ternary operator?
                    Console.ForegroundColor = validAcctNum ? ConsoleColor.Green : ConsoleColor.Red;
                    Console.WriteLine("Valid Account Number: " + validAcctNum);
                    Console.WriteLine(""); //newline for pretty console output
                    Console.ForegroundColor = ConsoleColor.White;

                    //grab next three lines
                    inputLine1 = inputStream.ReadLine();
                    inputLine2 = inputStream.ReadLine();
                    inputLine3 = inputStream.ReadLine();
                }

            }
            Console.ReadKey();

        }

        //performs the checksum calculation
        static public bool checkSum(char[] accountNum)
        {
            ///////////////////////////////////////////////////
            //checksum calculation:                          //
            //(d1 + 2 * d2 + 3 * d3 +..+ 9 * d9) mod 11 = 0  //
            ///////////////////////////////////////////////////

            Array.Reverse(accountNum); //reverse the array we pass in to make things a little easier on us

            int sum = 0;
            for (int i = 0; i < accountNum.Length; i++)
            {
                //convert each char to an int. must use GetNumericValue since we are dealing with ASCII tables
                sum += (int)char.GetNumericValue(accountNum[i]) * (i + 1);
            }

            Console.WriteLine("CheckSum Total: " + sum);

            if (sum % 11 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //decodes our three strings into a stringbuilder obj
        static public StringBuilder getChar(string line1, string line2, string line3, StringBuilder formattedNumber)
        {
            int i = 0;
            string unformattedNumber = "";

            while (i <= line1.Length - 3) //don't want to fall off the end of the string after we get the 24th char + 3
            {
                string subLine1 = line1.Substring(i, 3);
                string subLine2 = line2.Substring(i, 3);
                string subLine3 = line3.Substring(i, 3);

                unformattedNumber = subLine1 + subLine2 + subLine3;

                char[] bitmap = new char[9];
                bitmap = charToArray(unformattedNumber);

                char digit;
                digit = charToDigit(bitmap);

                i = i + 3; //skip to the start of the next three chars that represent a number

                formattedNumber.Append(digit);
            }

            return formattedNumber;

        }

        //This is a quality of life/readability function that returns an array of 0's and 1's that represent a blank 
        //space or a pipe/underscore in the unformatted account number.  Technically, it is unecessary. 
        //This fxn makes manipulating the case statement in the charToDigit fxn far easier to read/handle.
        //If machine resources were low we would have to cut this fxn and deal with an uglier case statement.
        static public char[] charToArray(string unformattedNumber)
        {
            char[] bitmap = new char[9]; //store the nine 
            int i = 0;

            //we store a 1 where there is a symbol, a 0 where there is an empty space
            foreach (char c in unformattedNumber)
            {
                if (c != ' ')
                {
                    bitmap[i] = '1';
                }
                else 
                {
                    bitmap[i] = '0';
                }
                i++;
            }
            //charToDigit(bitmap);

            return bitmap;
        }

        public static char charToDigit(char[] bitmap)
        {
            string arrString = new string(bitmap); //convert bitmap to string for switch statement
            char digit; //actual value we will return

            //determines what number we are actually working with
            switch (arrString)
            {
                case "010101111":
                    digit = '0';
                    break;
                case "000001001": //1
                    digit = '1';
                    break;
                case "010011110": //2
                    digit = '2';
                    break;
                case "010011011": //3
                    digit = '3';
                    break;
                case "000111001": //4
                    digit = '4';
                    break;
                case "010110011": //5
                    digit = '5';
                    break;
                case "010110111": //6 
                    digit = '6';
                    break;
                case "010001001": //7
                    digit = '7';
                    break;
                case "010111111": //8
                    digit = '8';
                    break;
                case "010111011": //9
                    digit = '9';
                    break;
                
                //This default case could be used for sanity checks before validation. If none of the cases are satisfied,
                //make the default digit = X, and then check that account numbers don't contain an X along with checkSum validation.
                default:
                    digit = '0';
                    break;
            }

            return digit;
        }
    }
}
